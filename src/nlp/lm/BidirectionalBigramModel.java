package nlp.lm;

import java.io.File;
import java.util.List;

public class BidirectionalBigramModel {

	BackwardBigramModel bbm;

	BigramModel bm;

	public BidirectionalBigramModel(BigramModel bm, BackwardBigramModel bbm) {
		this.bbm = bbm;
		this.bm = bm;
	}

	public void test2(List<List<String>> sentences) {
		double totalLogProb = 0;
		double totalNumTokens = 0;
		for (List<String> sentence : sentences) {
			totalNumTokens += sentence.size();
			double sentenceLogProb = (bm.sentenceLogProb2(sentence)
					+ bbm.sentenceLogProb2(sentence)) * 0.5;
			totalLogProb += sentenceLogProb;
		}
		double perplexity = Math.exp(-totalLogProb / totalNumTokens);
		System.out.println("Word Perplexity = " + perplexity);
	}

	public static void main(String[] args) {
		// All but last arg is a file/directory of LDC tagged input data
		File[] files = new File[args.length - 1];
		for (int i = 0; i < files.length; i++)
			files[i] = new File(args[i]);
		// Last arg is the TestFrac
		double testFraction = Double.valueOf(args[args.length - 1]);
		// Create a bigram model and train it.
		BigramModel model = new BigramModel();
		model.trainModel(files, testFraction);
		BackwardBigramModel backwardModel = new BackwardBigramModel(model);
		BidirectionalBigramModel bibm = new BidirectionalBigramModel(model,
				backwardModel);
		bibm.test2(model.trainSentences);
		System.out.println("Testing...");
		bibm.test2(model.testSentences);
	}
}
