package nlp.lm;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class BackwardBigramModel {

	public BackwardBigramModel(BigramModel bm) {
		this.setBm(bm);
	}

	public BigramModel getBm() {
		return bm;
	}

	public void setBm(BigramModel bm) {
		this.bm = bm;
	}

	/**
	 * Use sentences as a test set to evaluate the model. Print out perplexity
	 * of the model for this test data
	 */
	public void test(List<List<String>> sentences) {
		// Compute log probability of sentence to avoid underflow
		double totalLogProb = 0;
		// Keep count of total number of tokens predicted
		double totalNumTokens = 0;
		// Accumulate log prob of all test sentences
		for (List<String> sentence : sentences) {
			// Num of tokens in sentence plus 1 for predicting </S>
			totalNumTokens += sentence.size() + 1;
			// Compute log prob of sentence
			double sentenceLogProb = sentenceLogProb(sentence);
			// System.out.println(sentenceLogProb + " : " + sentence);
			// Add to total log prob (since add logs to multiply probs)
			totalLogProb += sentenceLogProb;
		}
		// Given log prob compute perplexity
		double perplexity = Math.exp(-totalLogProb / totalNumTokens);
		System.out.println("Perplexity = " + perplexity);
	}

	/* Compute log probability of sentence given current model */
	public double sentenceLogProb(List<String> sentence) {
		// Set start-sentence as initial token
		String prevToken = "</S>";
		// Maintain total sentence prob as sum of individual token
		// log probs (since adding logs is same as multiplying probs)
		double sentenceLogProb = 0;
		// Check prediction of each token in sentence
		for (int j = sentence.size() - 1; j >= 0; j--) {
			String token = sentence.get(j);
			// Retrieve unigram prob
			DoubleValue unigramVal = bm.unigramMap.get(token);
			if (unigramVal == null) {
				// If token not in unigram model, treat as <UNK> token
				token = "<UNK>";
				unigramVal = bm.unigramMap.get(token);
			}
			// Get bigram prob
			String bigram = bm.bigram(token, prevToken);
			DoubleValue bigramVal = bm.bigramMap.get(bigram);
			// Compute log prob of token using interpolated prob of unigram and
			// bigram
			double logProb = Math
					.log(bm.interpolatedProb(unigramVal, bigramVal));
			// Add token log prob to sentence log prob
			sentenceLogProb += logProb;
			// update previous token and move to next token
			prevToken = token;
		}
		// Check prediction of end of sentence token
		DoubleValue unigramVal = bm.unigramMap.get("<S>");
		String bigram = bm.bigram("<S>", prevToken);
		DoubleValue bigramVal = bm.bigramMap.get(bigram);
		double logProb = Math.log(bm.interpolatedProb(unigramVal, bigramVal));
		// Update sentence log prob based on prediction of </S>
		sentenceLogProb += logProb;
		return sentenceLogProb;
	}

	/**
	 * Like test1 but excludes predicting end-of-sentence when computing
	 * perplexity
	 */
	public void test2(List<List<String>> sentences) {
		double totalLogProb = 0;
		double totalNumTokens = 0;
		for (List<String> sentence : sentences) {
			totalNumTokens += sentence.size();
			double sentenceLogProb = sentenceLogProb2(sentence);
			// System.out.println(sentenceLogProb + " : " + sentence);
			totalLogProb += sentenceLogProb;
		}
		double perplexity = Math.exp(-totalLogProb / totalNumTokens);
		System.out.println("Word Perplexity = " + perplexity);
	}

	/**
	 * Like sentenceLogProb but excludes predicting end-of-sentence when
	 * computing prob
	 */
	public double sentenceLogProb2(List<String> sentence) {
		String prevToken = "</S>";
		double sentenceLogProb = 0;
		for (int j = sentence.size() - 1; j >= 0; j--) {
			String token = sentence.get(j);
			DoubleValue unigramVal = bm.unigramMap.get(token);
			if (unigramVal == null) {
				token = "<UNK>";
				unigramVal = bm.unigramMap.get(token);
			}
			String bigram = bm.bigram(token, prevToken);
			DoubleValue bigramVal = bm.bigramMap.get(bigram);
			double logProb = Math
					.log(bm.interpolatedProb(unigramVal, bigramVal));
			sentenceLogProb += logProb;
			prevToken = token;
		}
		return sentenceLogProb;
	}

	public static BackwardBigramModel trainModel(BigramModel bm) {
		return new BackwardBigramModel(bm);
	}

	public static void main(String[] args) throws IOException {
		// All but last arg is a file/directory of LDC tagged input data
		File[] files = new File[args.length - 1];
		for (int i = 0; i < files.length; i++)
			files[i] = new File(args[i]);
		// Last arg is the TestFrac
		double testFraction = Double.valueOf(args[args.length - 1]);
		// Create a bigram model and train it.
		BigramModel model = new BigramModel();
		model.trainModel(files, testFraction);
		BackwardBigramModel backwardModel = new BackwardBigramModel(model);
		// Test on training data using test and test2
		backwardModel.test(model.trainSentences);
		backwardModel.test2(model.trainSentences);
		System.out.println("Testing...");
		// Test on test data using test and test2
		backwardModel.test(model.testSentences);
		backwardModel.test2(model.testSentences);
	}

	private BigramModel bm;

}
